This application offers a small REST API for users and films.  
A user may attach a rating and a comment to each film. This application then is able to return the films ordered by
rating.

**Prerequisites**  

1. Node.js
2. NPM
3. MySQL

**Quick Start**

1. Use your mysql client to create the database: `CREATE DATABASE filmProject;`
2. Use your mysql client to create a new user by running `CREATE USER 'filmUser'@'localhost' IDENTIFIED BY '3209';` and
grant this user all permissions for new your database with the command `GRANT ALL PRIVILEGES ON filmProject.* to 
'filmUser'@'localhost';`. Save your changes by running `FLUSH PRIVILEGES;`.
3. `cd YOUR_PROJECT_DIR`
4. `npm install` (install dependencies)
5. `node index.js` (starts application)
6. Now you can access the REST API at http://localhost:3000/ (e.g. with Postman). Please read the documentation for 
more information about the REST API.

**Important Notes**
* You need to set the token on the "Authentication" header with the format "Bearer YOUR_TOKEN" to authenticate yourself.
