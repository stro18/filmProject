const express = require("express");
const bodyParser = require("body-parser");
const passport = require("./src/Services/authenticator").passport;
const userRouter = require("./src/Controller/UserController");
const filmRouter = require("./src/Controller/FilmController");
const authRouter = require("./src/Controller/AuthenticationController");
const User = require("./src/Entities/User");
const Film = require("./src/Entities/Film");

const app = express();
const port = 3000;

// Asynchronous; important to sync film table with user-film association
Film.sync()
  .then(() => console.log("Film table updated successfully"))
  .catch(err => console.error("Error during table creation"));

// Creates asynchronous a user table if non exists
User.sync()
  .then(() => console.log("User table updated successfully"))
  .catch(err => console.error("Error during table creation"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());

app.use("", authRouter);

app.use("", userRouter);
app.use("", filmRouter);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
