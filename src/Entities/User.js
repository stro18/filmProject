const express = require("express");
const sequelize = require("../../config/sequelize");

/**
 * Model of user entity
 */
class User extends sequelize.sequelizeClass.Model {}

User.init(
  {
    // id is automatically added
    username: {
      type: sequelize.sequelizeClass.STRING,
      allowNull: false
    },
    email: {
      type: sequelize.sequelizeClass.STRING,
      allowNull: false,
      unique: true
    },
    passwordHash: {
      type: sequelize.sequelizeClass.STRING,
      allowNull: false
    }

    // createdAt and updatedAt attributes are added automatically
  },
  { sequelize: sequelize.sequelizeInstance }
);

// Film needs User
module.exports = User;
const Film = require("./Film");

// creates database methods getFilms and setFilms
User.hasMany(Film, { as: "Films" });

module.exports = User;
