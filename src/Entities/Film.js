const express = require("express");
const sequelize = require("../../config/sequelize");
const User = require("./User");

const router = express.Router();

/**
 * Model of film entity
 */
class Film extends sequelize.sequelizeClass.Model {}

Film.init(
  {
    // id ia added automatically
    title: {
      type: sequelize.sequelizeClass.STRING,
      allowNull: false
    },
    rating: {
      type: sequelize.sequelizeClass.INTEGER
    },
    comment: {
      type: sequelize.sequelizeClass.STRING
    }

    // createdAt and updatedAt attributes are added automatically
  },
  { sequelize: sequelize.sequelizeInstance }
);

module.exports = Film;
