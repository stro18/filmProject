const express = require("express");
const authenticator = require("../Services/authenticator");

const router = express.Router();

/**
 * login route, returns JWT
 *
 * @name login
 * @route {POST} /login
 * @bodyparam email
 * @bodyparam password
 */
router.post("/login", async function(req, res, next) {
  const { email, password } = req.body;
  authenticator
    .authenticate(email, password)
    .then(token => res.json({ msg: "ok", token }))
    .catch(() => res.status(401).json({ msg: "Wrong email or password!" }));
});

// Secures film endpoints
router.all(
  "/film",
  authenticator.passport.authenticate("jwt", { session: false }),
  function(req, res, next) {
    next();
  }
);

module.exports = router;
