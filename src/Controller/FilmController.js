const express = require("express");
const User = require("../Entities/User");
const Film = require("../Entities/Film");
const databaseManager = require("../Services/databaseManager");

const router = express.Router();

/**
 * route for adding one film
 *
 * @name Post film
 * @route {POST} /film
 * @authentication user has to be logged in
 * @headerparam Authorization contains JWT
 * @bodyparam title
 * @bodyparam rating
 * @bodyparam comment
 */
router.post("/film", function(req, res) {
  const { title, rating, comment } = req.body;
  // then and catch expect function as parameter, not invocation
  databaseManager
    .persistFilm(req.user.id, { title, rating, comment })
    .then(film => res.json({ film, msg: "Film created successfully" }))
    .catch(err =>
      res.status(400).json({ msg: "Failure: Could not persist film!" })
    );
});

/**
 * route for getting all films, ordered by rating
 *
 * @name Get filmCollection
 * @route {GET} /film
 * @authentication user has to be logged in
 * @headerparam Authorization contains JWT
 * @queryparam {boolean} [filter=false] if true, server response contains only films of the user making the request
 * @queryparam {int} [limit] maximum number of returned films
 */
router.get("/film", function(req, res) {
  databaseManager
    .retrieveAllFilms(req.query.filter, req.user.id, req.query.limit)
    .then(films => res.json({ films, msg: "Films successfully retrieved" }))
    .catch(err =>
      res.status(400).json({ msg: "Failure: Could not retrieve films!" })
    );
});

/**
 * route for updating one film
 *
 * @name Update film
 * @route {PUT} /film
 * @authentication user has to be owner of film
 * @headerparam Authorization contains JWT
 * @bodyparam id id of film to be updated
 * @bodyparam title
 * @bodyparam rating
 * @bodyparam comment
 */
router.put("/film", function(req, res) {
  const { id, title, rating, comment } = req.body;

  databaseManager
    .updateFilm(req.user.id, { id, title, rating, comment })
    .then(newFilm => res.json({ newFilm, msg: "Film successfully updated." }))
    .catch(err =>
      res.status(400).json({ msg: "Failure: Could not update film!" })
    );
});

module.exports = router;
