const express = require("express");
const User = require("../Entities/User");
const databaseManager = require("../Services/databaseManager");
const router = express.Router();

/**
 * route for registering
 *
 * @name Post user
 * @route {POST} /user
 * @bodyparam username
 * @bodyparam email
 * @bodyparam password
 */
router.post("/user", function(req, res, next) {
  const { username, email, password } = req.body;
  databaseManager.persistUser({ username, email, password }).then(user => {
    user.passwordHash = undefined;
    res.json({ user, msg: "account created successfully" });
  });
});

module.exports = router;
