const bcrypt = require("bcrypt");
const User = require("../Entities/User");
const Film = require("../Entities/Film");

/**
 * @namespace
 */
var databaseManager = {};

/** @memberof databaseManager */
function persistUser({ username, email, password }) {
  return bcrypt
    .hash(password, 12)
    .then(passwordHash => User.create({ username, email, passwordHash }));
}

databaseManager.persistUser = persistUser;

/** @memberof databaseManager */
function persistFilm(userId, { title, rating, comment }) {
  return User.findOne({ where: { id: userId } }).then(async user => {
    // If user not found, findOne returns null
    if (!user) {
      return Promise.reject();
    }
    let film = await Film.create({ title, rating, comment });
    let films = await user.getFilms();
    // returns length of array
    films.push(film);
    user.setFilms(films);
    return film;
  });
}

databaseManager.persistFilm = persistFilm;

/** @memberof databaseManager */
function retrieveAllFilms(filter, userId, limit) {
  let param = {};
  param.order = [["rating", "DESC"]];
  if (filter) {
    param.where = { UserId: userId };
  }
  if (limit) {
    param.limit = parseInt(limit);
  }
  return Film.findAll(param);
}

databaseManager.retrieveAllFilms = retrieveAllFilms;

/** @memberof databaseManager */
function updateFilm(userId, { id, title, rating, comment }) {
  return Film.findOne({ where: { id } }).then(film => {
    if (film.UserId == userId) {
      return film.update({ title, rating, comment });
    } else {
      return Promise.reject();
    }
  });
}

databaseManager.updateFilm = updateFilm;

module.exports = databaseManager;
