// Not implemented yet

import { beforeMethod, Advised } from "aspect.js";

class LoggerAspect {
  @beforeMethod({
    classNamePattern: / /,
    methodNamePattern: / /
  })
  invokeBeforeMethod(meta) {
    console.log(
      `Inside of the logger. Called ${meta.className}.${
        meta.method.name
      } with args: ${meta.method.args.join(", ")}.`
    );
  }
}
