// important source: https://medium.com/devc-kano/basics-of-authentication-using-passport-and-jwt-with-sequelize-and-mysql-database-748e09d01bab
// second source: https://github.com/auth0/node-jsonwebtoken

const express = require("express");
const passport = require("passport");
const passportJWT = require("passport-jwt");
const jwt = require("jsonwebtoken");
const User = require("../Entities/User");
const bcrypt = require("bcrypt");

/**
 * @namespace
 */
var authenticator = {};

// ExtractJWT helps to extract the token
let ExtractJwt = passportJWT.ExtractJwt;

// JwTStrategy is the constructor for the strategy of the authentification
let JwtStrategy = passportJWT.Strategy;

let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = "wuwhdzb4r4637534nfejj";
jwtOptions.passReqToCallback = true;

// create our strategy
let strategy = new JwtStrategy(jwtOptions, function(req, jwt_payload, done) {
  // login route is configured to set userid as jwtPayload (the payload also contains the iat (issued at) and the exp of the token)
  let user = User.findOne({ where: { id: jwt_payload.id } })
    .then(user => {
      req.user = { id: jwt_payload.id };
      done(null, user);
    })
    .catch(() => done(null, false, { message: "JWT is not valid!" }));
});

// Apply strategy to passport authenfication
passport.use(strategy);

authenticator.passport = passport;

/** @memberof authenticator */
async function authenticate(email, password) {
  if (email && password) {
    let user = await User.findOne({ where: { email } });
    if (!user) {
      return Promise.reject();
    }

    // the salt is stored alongside the passwordHash,that's why you can compare both passwords
    let isPasswordTrue = await bcrypt.compare(password, user.passwordHash);
    if (isPasswordTrue == true) {
      // from now on we'll identify the user by id
      let payload = { id: user.id };
      // token expiration time is 1 hour
      return jwt.sign(payload, jwtOptions.secretOrKey, { expiresIn: 60 * 60 });
    } else {
      return Promise.reject();
    }
  } else {
    return Promise.reject();
  }
}

authenticator.authenticate = authenticate;

module.exports = authenticator;
